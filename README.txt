*******************************************************************************
CAUTION: Enabling Haubergeon without providing configuration in settings.php
will result in an inaccessible site.
*******************************************************************************

Haubergeon provides some defense in depth by several mechanisms:

- Enforce IP-restriction on the use of certain roles (thus guarding against
  elevation by gaining a role or taking over a user account)
- Forbid access to non-whitelisted paths.
- Forbid access to blacklisted paths.

The module doesn't modify access callbacks or calls to user_access, but
listens early in the request and terminates if certain conditions are met.

Vulnerabilities that occur pre-hook_boot execution and, depending on the
haubergeon_drupal_error setting, those in drupal_not_found() can still be
exploited. In the latter case Haubergeon does its best to neuter
$_POST/$_GET/$_REQUEST.

Configuration variables for use in settings.php as $conf['variable']:


- haubergeon_drupal_error

Choose between drupal_not_found creating a rendered 404 (TRUE) or the page defined in
'404_fast_html' (FALSE). The latter is the more secure and default option.


- haubergeon_limit_role_to_ip

Ensure certain roles can only be used from a particular IP-address or IP-range.
An array of arrays keyed on role id (RID). Simple IP ranges can be provided
with the IP-IP syntax. The approved range includes the min and max IP given.

An empty array for a role will result in no user with that role being able to
access the site. A NULL value (or not being defined) will allow use of that
role from any IP.

$conf['haubergeon_limit_role_to_ip'] = array(
  3 => array('192.168.1.10', '192.168.2.50-192.168.2.100'),
  4 => array(),
  5 => NULL,
)


- haubergeon_exempt

An array of IP addresses or IP ranges. Requests from these IPs are not subject
to the path whitelist and path blacklist.

Example:

$conf['haubergeon_exempt'] = array(
  '127.0.0.1', // Drush
  '192.168.1.10', '192.168.2.50-192.168.2.100',
);


- haubergeon_path_whitelist

The role based whitelist. An array of arrays keyed on role ID (RID). The
values consists of regular expressions. These expressions will be delimited
and anchored by Haubergeon.

The actual regular expression executed by Haubergeon is "#^YOURVALUE$#i.

The whitelist for the anonymous role will be automatically included in the
whitelist for authenticated users.

CAUTION: Make sure to define exempt IPs with haubergeon_exempt if you do not
whitelist certain paths required for login and password reset functionality!

Suggested starter configuration:

$conf['haubergeon_path_whitelist'] = array(
  DRUPAL_ANONYMOUS_RID => array(
    '/', // front page
    "user",
    "user/login",
    "user/password",
    "user/reset/\d+/\d+/.*",
    "sites/default/files/styles/.*", // For initial requests to imagecache.
  ),
  DRUPAL_AUTHENTICATED_RID => array(
    "user/logout",
    "user/%uid%",
    "user/%uid%/edit",
  ),
);

If you only want to use a blacklist, you MUST make a blanket whitelist rule for
the anonymous role DRUPAL_ANONYMOUS_RID:

$conf['haubergeon_path_whitelist'] = array(
  DRUPAL_ANONYMOUS_RID => array('.*'),
);


- haubergeon_path_blacklist - a role based path blacklist

$conf['haubergeon_path_blacklist'] = array(
  DRUPAL_ANONYMOUS_RID => array('admin', 'admin/.*'),
);

CAUTION: Make sure to define exempt IPs if you blacklist certain paths required
for login and password reset functionality.


- haubergeon_ignore_authenticated_rid_blacklist

This variable controls whether the restrictions for DRUPAL_AUTHENTICATED_RID
apply to all authenticated users, or just those with no additional roles. The
default FALSE also enforces these limits on user accounts with additional roles.
Set to TRUE if you want to exclude the DRUPAL_AUTHENTICATED_RID blacklist for
users with additional roles.

$conf['haubergeon_ignore_authenticated_rid_blacklist'] = TRUE;


- haubergeon_enable_log

Determines whether to log to the error_log. This is useful when
enumerating paths to whitelist. Defaults to TRUE (to help admins who enabled
Haubergeon without configuring it).


TODO

Functionality: Trusted proxies.
Documentation: Caveats, Page cache, Reverse proxies. +Add role to uid 1 to tie to IP, or disable account & create role.
